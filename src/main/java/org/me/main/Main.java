package org.me.main;

import java.io.*;
import java.time.Duration;
import java.time.LocalTime;


public class Main {
        public static void main(String[] args) throws IOException {
                int maxCharCount = args.length > 0 ? Integer.valueOf(args[0]) : -1;
                String path = "text.txt";
                String out = "out.txt";

                LocalTime time = LocalTime.now();

                FrequencyCalculator calc = new FrequencyCalculator(path);
                calc.printFrequency(out, maxCharCount);

                System.out.println("Elapsed time "+Duration.between(time, LocalTime.now()).toMillis() + " ms");
        }
}

