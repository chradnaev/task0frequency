package org.me.main;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;


/**
 * Class
 */
public class FrequencyCalculator {
        private Map<String, Integer> frequencyMap = new TreeMap<>();
        private int totalCount;
        private final int BUFFER_SIZE = 4096;

        private void readString(String path)  throws IOException {

                try (BufferedReader reader  = new BufferedReader(new InputStreamReader(new FileInputStream(path),
                                StandardCharsets.UTF_8),BUFFER_SIZE)) {
                        char[] buffer = new char[BUFFER_SIZE];
                        while (reader.ready()) {
                                int charCount = reader.read(buffer);
                                countFrequency(new String(buffer, 0, charCount));
                                totalCount += charCount;
                        }

                }
        }

        private void countFrequency(String st) {

                for (int i=0; i < st.length(); i++) {
                        String character = st.substring(i,i+1);

                        if (frequencyMap.containsKey(character)) {
                                frequencyMap.put(character, frequencyMap.get(character) + 1);
                        }
                        else {
                                frequencyMap.put(character, 1);
                        }
                }
        }

        public void printFrequency(String out, int maxCharCount) throws IOException {

                try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out),
                                StandardCharsets.UTF_8),BUFFER_SIZE)) {
                        float full = 0;
                        float percent;
                        int charCount = 1;

                        for (Map.Entry<String, Integer> entry : frequencyMap.entrySet()) {
                                percent = (float) entry.getValue() * 100 / totalCount;
                                full += percent;
                                String character = entry.getKey()
                                        .replace(" ","space")
                                        .replace("\n","\\n");
                                String result = String.format("%5s(%2.1f%%): %s", character, percent,
                                        repeatedCharacter('#', Math.round(percent)));
                                System.out.println(result);
                                writer.write(result + "\n");
                                if ((charCount >= maxCharCount) && (maxCharCount != -1))
                                        break;
                                charCount++;
                        }
                        System.out.println("Total percents: " + String.valueOf(full) + "%");
                }
        }

        private static String repeatedCharacter(Character st, int count) {
                StringBuilder result = new StringBuilder();

                for (int i = 0; i < count; i++) {
                        result.append(st);
                }

                return result.toString();
        }

        /**
         * Init class from string file
         * @param path path to file
         * @throws IOException
         */
        public FrequencyCalculator(String path) throws IOException {
                readString(path);
        }
}
